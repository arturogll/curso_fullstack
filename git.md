# Curso git

Instalar git en Arch

```
sudo pacman -S git
```

Clonar repositorio

```
git clone https://gitlab.com/arturogll/test_git.git
```

### status

```
git status
```
### add
```
git add .
```
### commit 

```
git commit -m "mensaje"
```
### push

```
git push origin master
```

### obtener el log

```
git log
```

### cambiar de rama

```
git checkout
```

Evaluacion

- Crear un repositorio y clonarlo
- crear un primer commit con un archivo con texto
- crear una carpeta con un archivo dentro con texto y hacer otro commit
- agregar una imagen a la carpeta raiz del repositorio y hacer commit
- regresar al primer commit 
- regresar al segundo
- regresar al tercero
